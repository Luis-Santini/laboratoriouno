package com.santini.laboratoriouno

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.IntegerRes
import androidx.core.text.set
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnProcesar.setOnClickListener {

            val parsedInt = edtEdad.text.toString().toIntOrNull()

            if (parsedInt != null) {
                if (parsedInt >= 18) {
                    tvResultado.text = "Usted es mayor de edad"
                } else {
                    tvResultado.text = "Usted es menor de edad"
                }
            } else {
                Toast.makeText(this, "Debe ingresar un valor en el campo edad", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}




